#include "arp.h"
#include "utility.h"

void arp::arpRecv(uint8_t* buf)
{
    header.hwType    = consume<uint16_t>(buf);
    header.protType  = consume<uint16_t>(buf);
    header.hwSize    = consume<decltype(header.hwSize)>(buf);
    header.protSize  = consume<decltype(header.protSize)>(buf);
    header.opcodeReq = consume<uint16_t>(buf);
    consume(buf,  header.senderMacAddr);
    consume(buf,  header.senderIpAddr);
    consume(buf,  header.targetMacAddr);
    consume(buf,  header.targetIpAddr);

}