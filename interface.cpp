#include "interface.h"
#include <fcntl.h>  /* O_RDWR */
#include <unistd.h> /* read(), close() */
#include <linux/if_tun.h>
#include <sys/ioctl.h> /* ioctl() */

#include <sys/socket.h>
#include <linux/if.h>
#include <cstring>
#include "utility.h"

interface::interface()
{
  
}

interface& interface::getInterface()
{
    static interface i;

    return i;
}

int interface::initInterface(std::string interfaceAddress , 
                             std::string interfaceRouteAddress , 
                             std::string interfaceName)
{
    int err = 0;
    int interfaceInitStep = 1;
 
    tapAddr  = interfaceAddress; 
    tapRoute = interfaceRouteAddress;
    devName  = interfaceName;

    switch(interfaceInitStep)
    {
        case 1:
            err = openTapDevice();
            if(err != 0)
            {
                break;
            }
        case 2:
            err = setIfUp();
            if(err != 0)
            {
                break;
            }
        case 3:
            err = setIfRoute();
            if(err != 0)
            {
                break;
            }
        case 4:
            err = setIfAddress();
            if(err != 0)
            {
                break;
            }

        default:
            break;
    }

    return err;
}


int interface::openTapDevice()
{
    struct ifreq ifr{};
    fd  = std::move(open(devPath.c_str(), O_RDWR));
    auto err = 0;


    ifr.ifr_flags = IFF_TAP| IFF_NO_PI;;
    strncpy(ifr.ifr_name, devName.c_str(), IFNAMSIZ); // devname = "tun0" or "tun1", etc 


    /* ioctl will use ifr.if_name as the name of TUN 
    * interface to open: "tun0", etc. */
    if ((err = ioctl(fd, TUNSETIFF, (void *) &ifr)) == -1) 
    {
        close(fd);
    }

    return err;
}

int interface::setIfRoute()
{
    std::string cmd = "ip route add dev";
    cmd = cmd + " " + devName + " " + tapRoute;

    return run_cmd(cmd);
}


int interface::setIfAddress()
{
    std::string cmd = "ip address add dev";

    cmd = cmd + " " + devName + " local " + tapAddr; 
    return run_cmd(cmd);
}


int interface::setIfUp()
{
    std::string cmd = "ip link set dev";
    cmd = cmd + " " + devName + " up";
    return run_cmd(cmd);
}


int interface::readInterfaceBuf(uint8_t* incomingBuffer)
{
    
    auto nbytes = read(fd, incomingBuffer, INCOMING_BUFFEER_LEN);

   
    return nbytes;
}
int interface::writeInterfaceBuf(const std::string& incomingBuffer)
{
    char buff[INCOMING_BUFFEER_LEN]{};
    
    memcpy(buff, incomingBuffer.c_str(), incomingBuffer.length() + 1);

    auto nbytes = write(fd, buff, incomingBuffer.length());
    
    return nbytes;
}