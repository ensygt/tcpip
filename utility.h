#pragma once

#include <string>
#include <cstdlib> 
#include <array>

int run_cmd(std::string cmd);

uint32_t ntoh(uint32_t value);

uint16_t ntoh(uint16_t value);

uint8_t ntoh(uint8_t value);

template <typename T>
T consume(uint8_t*& ptr) {
        T ret = *(reinterpret_cast<T*>(ptr));
        ptr += sizeof(T);
        return ntoh(ret);
}

void consume(uint8_t * &buf, std::array<uint8_t, 6> & mac);
void consume(uint8_t * &buf, std::array<uint8_t, 4> & ip);
