#pragma once

#include <string>
#include <vector>
class interface
{
    std::string tapAddr; 
    std::string tapRoute;
    std::string devName;
    std::string devPath{"/dev/net/tap"};
    const size_t INCOMING_BUFFEER_LEN = 2000;

    int fd;
    int openTapDevice();
    int setIfRoute();
    int setIfAddress();
    int setIfUp();

    interface();

public:
    interface(interface const&)       = delete;
    void operator=(interface const&)  = delete;
    static interface& getInterface();
    int initInterface(std::string interfaceAddress = "10.0.0.5", 
                      std::string interfaceRouteAddress = "10.0.0.0/24", 
                      std::string interfaceName = "tap0");
    int readInterfaceBuf(uint8_t* incomingBuffer);
    int writeInterfaceBuf(const std::string& buf);


};