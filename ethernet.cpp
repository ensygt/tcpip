
#include "ethernet.h"
#include "utility.h"

void ethernetv2::ethernetRecv(uint8_t *&buf)
{
    consume(buf,  header.destMacAddr);
    consume(buf,  header.sourceMacAddr);
    header.type  = consume<uint16_t>(buf);
}