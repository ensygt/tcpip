#include "utility.h"
#include "array"

int run_cmd(std::string cmd)
{
    return system(cmd.c_str());
}

uint32_t ntoh(uint32_t value) {
        return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
               (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
}

uint16_t ntoh(uint16_t value) { return (value & 0x00FF) << 8 | (value & 0xFF00) >> 8; }

uint8_t ntoh(uint8_t value) { return value; }




void consume(uint8_t * &buf, std::array<uint8_t, 6> & mac)
{
    for(int i = 0; i < 6; i++)
    {
        mac[i] = consume<uint8_t>(buf);
    }
}

void consume(uint8_t * &buf, std::array<uint8_t, 4> & ip)
{
    for(int i = 0; i < 4; i++)
    {
        ip[i] = consume<uint8_t>(buf);
    }
}