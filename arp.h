#pragma once

#include "arp_header.h"
#include <vector>
#include <cstdint>

class arp
{
    arp_header header;

    public:
        arp()  = default;
        ~arp() = default;
        void arpRecv(uint8_t* buf);
};  