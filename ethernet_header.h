#pragma once

#include <array>
#include <cstdint>

struct ethernet_header
{   
    std::array<uint8_t,6>  destMacAddr;
    std::array<uint8_t,6>  sourceMacAddr;
    uint16_t               type;
};