#pragma once
#include <array>
#include <cstdint>
struct arp_header
{   
    uint16_t hwType;
    uint16_t protType;
    uint8_t  hwSize;
    uint8_t  protSize;
    uint16_t opcodeReq;
    std::array<uint8_t,6> senderMacAddr;
    std::array<uint8_t,4>  senderIpAddr;
    std::array<uint8_t,6>  targetMacAddr;
    std::array<uint8_t,4>  targetIpAddr;
};